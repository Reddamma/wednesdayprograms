package Programs;

public class CountNumberOfCapitalSmallDigitsSpecialCharactersInString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "ADFsdcfgvbhjn12345678@#$%^&";
		int CapitalLetters = 0, SmallLetters = 0, Digits = 0, SpecialCharacters = 0;
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (ch >= 'A' && ch <= 'Z')
				CapitalLetters++;
			else if (ch >= 'a' && ch <= 'z')
				SmallLetters++;
			else if (ch >= '0' && ch <= '9')
				Digits++;
			else
				SpecialCharacters++;
		}
		System.out.println("Capital Letters" + CapitalLetters);
		System.out.println("Small letters : " + SmallLetters);
		System.out.println("Digits : " + Digits);
		System.out.println("Special characters : " + SpecialCharacters);
	}
}