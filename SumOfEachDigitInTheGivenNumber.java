package Programs;

public class SumOfEachDigitInTheGivenNumber {
	public static int Sum(int n) {
		int sum = 0;
		while (n != 0) {
			int rem=n%10;
			sum = sum+rem;
			n = n / 10;
		}
		return sum;
	}

	public static void main(String[] args) {
		int n = 573;
		System.out.println(Sum(n));
	}
}
